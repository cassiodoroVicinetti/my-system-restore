#!/bin/bash


echo -e "\n*** completamento installazione di VirtualBox ***"
sudo aptitude -y install virtualbox-ext-pack


echo -e "\n*** libdvd-pkg ***"
sudo aptitude -y install libdvd-pkg
sudo dpkg-reconfigure libdvd-pkg


echo -e "\n*** completamento installazione di Wine ***"
winecfg


echo -e "\n *** Xournal++ ***"
flatpak install flathub com.github.xournalpp.xournalpp
sudo chown luca:luca /var/lib/flatpak/app/com.github.xournalpp.xournalpp/current/active/export/share/applications/com.github.xournalpp.xournalpp.desktop


# echo -e "\n*** visualizzatori di Microsoft Office ***"
# echo "Da Integrazione della Scrivania, imposta il tema e le cartelle."
# WINEARCH=win32 WINEPREFIX=/home/luca/.wine32 winecfg
# WINEPREFIX=/home/luca/.wine32 wine regedit "/home/luca/Software/Programmi/Office/Visualizzatori/riched20.reg"
# WINEPREFIX=/home/luca/.wine32 wine "/home/luca/Software/Programmi/Office/Visualizzatori/Word Viewer 2003.exe"
# WINEPREFIX=/home/luca/.wine32 wine "/home/luca/Software/Programmi/Office/Visualizzatori/Excel Viewer 2007.exe"
# WINEPREFIX=/home/luca/.wine32 wine "/home/luca/Software/Programmi/Office/Visualizzatori/PowerPoint Viewer 2010.exe"
# WINEPREFIX=/home/luca/.wine32 wine "/home/luca/Software/Programmi/Office/Visualizzatori/Office Compatibility Pack.exe"


# echo -e "\n*** tavolozza colori di LibreOffice ***"
# if [ ! -d "/home/luca/.config/libreoffice/4/user/config/" ]
# then
#   libreoffice # la cartella non esiste se non è stato mai avviato
# fi
# mv "/home/luca/.config/libreoffice/4/user/config/standard.soc" "/home/luca/.config/libreoffice/4/user/config/standard.soc.old"
# cp "/home/luca/Software/Personalizzazione/LibreOffice - Tavolozza colori di Microsoft Office/standard.soc" "/home/luca/.config/libreoffice/4/user/config/"


echo -e "\n*** completamento installazione di Dropbox ***"
dropbox start -i


# echo -e "\n*** configurazione di onedrive ***"
# cp altro/onedrive/config ~/.config/onedrive/ # https://github.com/abraunegg/onedrive/blob/master/config
# onedrive --synchronize
