
  INSTALLAZIONI MANUALI

Wine:
- Finale
-- disattivare i Garritan Instruments
- PDFill PDF Tools: https://www.pdfill.com/pdf_tools_free.html
-- https://gitlab.winehq.org/wine/wine/-/wikis/Wine-Mono#prefix-local-install
-- non installare GhostScript
---------------------------------
- Avidemux: http://fixounet.free.fr/avidemux/download.html
- BIMP: https://github.com/alessandrofrancesconi/gimp-plugin-bimp
- JD-GUI: http://java-decompiler.github.io/
- kDrive: https://www.infomaniak.com/it/applicazioni/scarica-kdrive
- Master PDF Editor 4
- midicomp: https://github.com/markc/midicomp
- Video DownloadHelper Companion App: https://www.downloadhelper.net/install-coapp
- XAMPP: https://www.apachefriends.org/it/download.html#download-linux

---------------------------------
---------------------------------

   CONFIGURAZIONE APPLICAZIONI

AUDACITY
- Pseudo-Stereo: http://wiki.audacityteam.org/wiki/Nyquist_Effect_Plug-ins#Pseudo-Stereo
-- copiare in /usr/share/audacity/plug-ins
-- abilitare dall'interfaccia
---------------------------------
CLEMENTINE
- Generale > Riproduzione:
− Dissolvenza all'interruzione di una traccia
− Dissolvenza incrociata al cambio manuale di traccia
+ Utilizza i metadati del guadagno di riproduzione se disponibili
- Generale > Comportamento:
+ Colora di grigio i brani della scaletta non esistenti
L'utilizzo del menu per aggiungere un brano: Non iniziare mai la riproduzione
Al doppio clic su un brano: Aggiungi alla scaletta, Non iniziare mai la riproduzione
Quando salvi una scaletta, i percorsi dei file dovrebbero essere: Relativo
- Generale > Raccolta musicale:
aggiungere "Musica/Raccolta musicale"
- Interfaccia utente > Aspetto:
-- Immagine di sfondo: + Nessuna immagine di sfondo
-- Barre dell'atmosfera:
+ Disabilita la creazione della barra dell'atmosfera
− Mostra una barra dell'atmosfera nella barra di avanzamento della traccia
---------------------------------
DOLPHIN

- Generale:
− Mostra marcatore di selezione
Modalità di ordinamento: Naturale
- Avvio:
Mostra all'avvio: /home/luca
- Navigazione:
+ Apri gli archivi come cartelle
- Menu contestuale:
− Aggiungi alla collezione di JuK
− Aggiungi la fonte ad Akregator
+ Dropbox
+ Elimina
+ Git
− Leggi DVD con Dragon Player

Visualizza > Ordina per > − Prima le cartelle
Visualizza > Mostra pannelli > + Informazioni

Impostazioni > Configura le barre degli strumenti...
Su Indietro Avanti | Annulla Aggiorna | Icone Compatta Dettagli | Terminale Cerca Mostra_anteprime | Nuova_scheda Dividi
---------------------------------
GWENVIEW
Generale > Video: − Mostra i video
Generale > Sfondo a schermo intero: Nero
Vista immagini > Rotellina del mouse: Sfoglia
Vista immagini > Modalità di ingrandimento: + Ingrandisci le immagini più piccole
Vista immagini > Modalità di ingrandimento > Animazioni: Nessuno
---------------------------------
KATE
- Visualizza > Viste degli strumenti > − Mostra barre laterali
- Impostazioni > + Mostra la barra degli strumenti
- Impostazioni > Configura Kate:
-- Aspetto > Generale > Carattere dell'editor: Noto Sans, Regular, 13
-- Aspetto > Bordi: − Mostra un'anteprima del testo sulla barra di scorrimento
-- Aspetto > Bordi: − Mostra minimappa di scorrimento
-- Apri e salva > Generale > Formato dei file > Limite di lunghezza delle righe: Illimitata
---------------------------------
KILE
- Settings > Configure Kile > Tools > Build > PDFLaTeX > General:
-- Options: -synctex=1 -interaction=nonstopmode -shell-escape '%source'
---------------------------------
KONSOLE
Impostazioni > Configura Konsole > Barra delle schede / Divisori:
- Mostra: +Sempre
- Posizione: Sopra l'area dei terminali
- + Mostra il pulsante "Nuova scheda"
Impostazioni > Modifica il profilo attuale:
- Generale:
-- Comando: /usr/bin/fish
- Aspetto:
-- Bianco su nero
-- Carattere: Noto Mono
-- Dimensioni: 13,5 pt
Impostazioni > Configura le barre degli strumenti...
Nuova_scheda Dividi_vista_destra/sinistra Dividi_vista_alto/basso | Copia Incolla Trova Apri_gestore_dei_file
---------------------------------
LIBREOFFICE
- TexMaths: https://extensions.libreoffice.org/extensions/texmaths-1
- Strumenti > Opzioni... > LibreOffice Calc > Formula:
-- + Usa nomi di funzione inglesi
-- Excel 2007 e successivi: Chiedi
-- Foglio di calcolo ODF (non salvato da LibreOffice): Chiedi
---------------------------------
MUSESCORE
https://blog.bmarwell.de/en/musescore-distorted-sound-fix/
- Modifica > Preferenze... > Generale > Avvio programma:
+ Avvia vuoto
- Visualizza > Sintetizzatore > Fluid:
mettere il SoundFont SGM nella lista
---------------------------------
NETWORK-MANAGER-FORTISSLVPN
sudo service NetworkManager stop
altro/system-connections > /etc/NetworkManager/system-connections/
sudo service NetworkManager start

come sono stati costruiti:
dal pacchetto systemd-resolved: resolvectl
netstat -nr
---------------------------------
QT CREATOR
- Edit > Preferences > Kits > Qt Versions > /usr/bin/qmake6
- Edit > Preferences > Kits > Kits > Manual > Desktop (default) > Qt version: Qt 6.3.1 (System)
- Environment > System > Terminal: /usr/bin/konsole --new-tab -e
- Text Editor > Font & Colors: Noto Sans, 12, 105%
---------------------------------
THUNDERBIRD

- POSTA ELETTRONICA

luca.ghio@ik.me
entrata:
- Protocollo: IMAP
- Server: mail.infomaniak.com
- Porta: 993
- Sicurezza della connessione: SSL/TLS
- Metodo di autenticazione: Password normale
uscita:
- Server: mail.infomaniak.com
- Porta: 465
- Sicurezza della connessione: SSL/TLS
- Metodo di autenticazione: Password normale

artghio@tiscali.it
entrata:
- Protocollo: IMAP
- Server: imap.tiscali.it
- Porta: 993
- Sicurezza della connessione: SSL/TLS
- Metodo di autenticazione: Password crittata
uscita:
- Server: smtp.tiscali.it
- Porta: 465
- Sicurezza della connessione: SSL/TLS
- Metodo di autenticazione: Password crittata

l.ghio@anbima.it, redazioneweb.alessandriaasti@anbima.it, luca.ghio@bandacassine.org
entrata:
- Protocollo: IMAP
- Server: imap.gmail.com
- Porta: 993
- Sicurezza della connessione: SSL/TLS
- Metodo di autenticazione: OAuth2
uscita:
- Server: smtp.gmail.com
- Porta: 587
- Sicurezza della connessione: STARTTLS
- Metodo di autenticazione: Password normale

lghio@aslal.it
entrata:
- Protocollo: IMAP
- Server: outlook.office365.com
- Porta: 993
- Sicurezza della connessione: SSL/TLS
- Metodo di autenticazione: OAuth2
uscita:
- Server: smtp.office365.com
- Porta: 587
- Sicurezza della connessione: STARTTLS
- Metodo di autenticazione: Password normale

e.bianco1990@libero.it
entrata:
- Protocollo: IMAP
- Server: imapmail.libero.it
- Porta: 993
- Sicurezza della connessione: SSL/TLS
- Metodo di autenticazione: Password normale
uscita:
- Server: smtp.libero.it
- Porta: 465
- Sicurezza della connessione: SSL/TLS
- Metodo di autenticazione: Password normale

- RUBRICA

ghio.luca91@gmail.com
https://www.googleapis.com/carddav/v1/principals/ghio.luca91@gmail.com/lists/default/

l.ghio@anbima.it
https://www.googleapis.com/carddav/v1/principals/l.ghio@anbima.it/lists/default/

luca.ghio@aziendazero.piemonte.it
https://comunica.ruparpiemonte.it/dav/luca.ghio%40aziendazero.piemonte.it/Contacts/

luca.ghio@bandacassine.org
https://www.googleapis.com/carddav/v1/principals/luca.ghio@bandacassine.org/lists/default/

redazioneweb.alessandriaasti@anbima.it
https://www.googleapis.com/carddav/v1/principals/redazioneweb.alessandriaasti@anbima.it/lists/default/

- CALENDARIO

ghio.luca91@gmail.com
https://apidata.googleusercontent.com/caldav/v2/ghio.luca91%40gmail.com/events/

l.ghio@anbima.it
https://apidata.googleusercontent.com/caldav/v2/l.ghio%40anbima.it/events/

luca.ghio@aziendazero.piemonte.it
https://comunica.ruparpiemonte.it/dav/luca.ghio@aziendazero.piemonte.it/Calendar

luca.ghio@bandacassine.org
https://apidata.googleusercontent.com/caldav/v2/luca.ghio%40bandacassine.org/events/

redazioneweb.alessandriaasti@anbima.it
https://apidata.googleusercontent.com/caldav/v2/redazioneweb.alessandriaasti%40anbima.it/events/
---------------------------------
VLC
Audio: Modalità guadagno di riproduzione: Traccia
Ingresso / Codificatori > Codificatori audio > FluidSynth > Sorgenti sonore: /home/luca/Software/SoundFont/SGM-V2.01.sf2
Interfaccia > Interfacce principali > Qt: Volume massimo visualizzato 200
Scaletta: + Permetti l'esecuzione di una sola istanza
Video: − Mostra titolo del media nel video
---------------------------------
- Akonadi:
~/.config/akonadi/akonadiserverrc
StartServer=false
- Chrome: /usr/bin/google-chrome-stable --profile-directory=Default %U
- Dropbox: Preferenze > Generale: − Avvia Dropbox all'avvio del sistema
- Evince: https://bugs.launchpad.net/ubuntu/+source/evince/+bug/632599/comments/2
- Master PDF Editor: Sistema: − Crea un file di backup
- OpenJDK:
https://access.redhat.com/documentation/en-us/openjdk/11/html-single/release_notes_for_openjdk_11.0.17/index#literal_sha_1_literal_signed_jars
/etc/java-xx-openjdk/security/java.security
jdk.jar.disabledAlgorithms=MD2, MD5, RSA keySize < 1024, \
      DSA keySize < 1024
- VirtualBox:
cambiare il percorso delle macchine virtuali a "~/.VirtualBox VMs/"
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1039711#10
/etc/default/grub > GRUB_CMDLINE_LINUX_DEFAULT > aggiungere "kvm.enable_virt_at_load=0" > sudo update-grub

---------------------------------
---------------------------------

IMPOSTAZIONI DI SISTEMA

Aspetto > Colori: Brezza
---------------------------------
Aspetto > Schermata iniziale: Nessuna
---------------------------------
Spazio di lavoro > Comportamento generale > Facendo clic su file o cartelle: + Si selezionano
---------------------------------
Spazio di lavoro > Comportamento dello spazio di lavoro > Effetti del desktop:
Accessibilità:
− Ingrandimento
Aspetto:
− Accesso
+ Finestre traballanti
+ Foglio
+ Lampada magica
---------------------------------
Spazio di lavoro > Comportamento del desktop > Lati dello schermo:
angolo in alto a sinistra: Nessuna azione
lato sinistro: Nessuna azione
---------------------------------
Spazio di lavoro > Comportamento del desktop > Blocco dello schermo > Blocca lo schermo automaticamente:
− Dopo 5 minuti
---------------------------------
Spazio di lavoro > Avvio e spegnimento > Schermata di accesso (SDDM): Brezza
---------------------------------
Spazio di lavoro > Avvio e spegnimento > Sessioni del desktop:
Opzione di uscita predefinita: Spegni il computer
All'accesso: Avvia una sessione vuota
---------------------------------
Personalizzazione > Applicazioni > Applicazioni predefinite > Browser web: Firefox
---------------------------------
Hardware > Dispositivi di immissione > Tastiera > Hardware > Bloc Num all'avvio di Plasma: Spento
---------------------------------
Hardware > Dispositivi di immissione > Touchpad:
− Disabilita durante la digitazione
---------------------------------
Hardware > Schermo e video > Compositore:
Metodo di scalatura: Rapido
Motore di rendering: XRender
---------------------------------
Hardware > Gestione energetica > Risparmio energetico > Alimentatore:
Luminosità dello schermo: 40
Gestione degli eventi dei pulsanti > Quando viene chiuso lo schermo del portatile: Non fare niente
---------------------------------
Hardware > Gestione energetica > Risparmio energetico > Batteria:
Luminosità dello schermo: 40
− Sospendi la sessione
Gestione degli eventi dei pulsanti > Quando viene chiuso lo schermo del portatile: Non fare niente
---------------------------------
Hardware > Gestione energetica > Risparmio energetico > Batteria quasi scarica:
Luminosità dello schermo: 40
− Sospendi la sessione
Gestione degli eventi dei pulsanti > Quando viene chiuso lo schermo del portatile: Non fare niente
---------------------------------
Impostazioni di Desktop:
Immagine di sfondo:
- Disposizione: Vista delle cartelle
- Tipo di sfondo: Presentazione
- Posizionamento: Scalata e ritagliata
- Cambia ogni: 0 Ore 1 Minuti 0 Secondi
- Aggiungi cartella > /usr/share/wallpapers/
Posizione:
- + Elemento del pannello Risorse: Home
Icone:
- Disposizione: Colonne
+ Blocca sul posto
Ordinamento: Nome
− Prima le cartelle
− Finestre a comparsa di anteprima delle cartelle
− Miniature di anteprima

---------------------------------
---------------------------------

   ALTRO
   
Preferiti nel menu delle applicazioni:
- Firefox
- Konsole
- Dolphin
- Kate
- LibreOffice
- Frescobaldi
- Clementine
- KeePassXC
---------------------------------
Controllo volume per applicazione di PulseAudio:
https://wiki.gentoo.org/wiki/User:Feystorm#PulseAudio_per-application_volume_control
in "/etc/pulse/daemon.conf":
flat-volumes = no

---------------------------------
---------------------------------

  FIREFOX

estensioni:
CORS Everywhere
DownThemAll!
Plasma Integration
Social Video Downloader
Startpage - Private Search Engine
Transparent Standalone Images
TunnelBear VPN
uBlock Origin
User-Agent Switcher
Video Downloader professional
Video DownloadHelper
Violentmonkey
Web Archives
YouTube Video Downloader/YouTube HD Download

disattivate:
FirePHP (Official)
I don't care about cookies
Tab Notifier
Terms of Service; Didn't Read
---------------------------------
script Violentmonkey:
(- Dplay Download Script: http://danielegiudice.altervista.org/scaricare-i-video-di-dplay-it/ )
(- Google Book Downloader: https://web.archive.org/web/20140208004256/http://userscripts.org/scripts/show/37933 )
- La7.tv direct link: http://andrealazzarotto.com/2013/02/07/scaricare-i-video-del-portale-la7-tv/
- Rai.tv native video player and direct links: http://andrealazzarotto.com/2012/11/24/guardare-e-scaricare-i-video-di-rai-tv-e-rai-replay-anche-con-linux/
- Video.mediaset.it native video player and direct links: http://andrealazzarotto.com/2012/11/02/ottenere-i-link-diretti-ai-video-del-portale-video-mediaset/
- https://github.com/LibreScore/dl-librescore#userscript
---------------------------------
motori di ricerca:
- Startpage - English
- Wikipedia (en)
- Wikipedia (it)
- YouTube
- Google Traduttore
---------------------------------
about:config:
browser.bookmarks.autoExportHTML = true
browser.bookmarks.file = "/home/luca/Risorse remote/kDrive/Personali/bookmarks.html"
security.mixed_content.block_active_content = false
widget.use-xdg-desktop-portal.file-picker = 1
widget.use-xdg-desktop-portal.location = 1
widget.use-xdg-desktop-portal.mime-handler = 1
widget.use-xdg-desktop-portal.settings = 1
dom.menuitem.enabled = true

---------------------------------
---------------------------------

  CHROMIUM

estensioni:
(Google Cast)
Libnotify Notifications in Chrome
Videostream for Google Chromecast™

---------------------------------
---------------------------------

  DRIVER

stampante Brother DCP-197C:
https://support.brother.com/g/b/downloadlist.aspx?c=it&lang=it&prod=dcp197c_eu&os=128&flang=English
Driver Install Tool
