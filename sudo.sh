#!/bin/bash


echo -e "\n*** apt ***"
# http://web.archive.org/web/20230206030453/https://www.hiroom2.com/2016/05/18/ubuntu-16-04-auto-apt-update-and-apt-upgrade
systemctl disable apt-daily.service # disable run when system boot
systemctl disable apt-daily.timer # disable timer run
cp "altro/apt/20auto-upgrades" "/etc/apt/apt.conf.d/"

cp "altro/apt/80_autoclean_installed" "/etc/apt/apt.conf.d/" # https://sleeplessbeastie.eu/2017/10/09/how-to-clear-the-apt-cache/#remove-outdated-packages-from-the-cache

cp "altro/apt/apt.conf" "/etc/apt/" # http://unix.stackexchange.com/a/8844/123169 https://askubuntu.com/questions/521520/inverse-of-install-suggests-option-in-apt-get-remove
mkdir "/mnt/apt"
mount -o loop "/home/luca/Software/Sistemi operativi/Linux/Debian/debian-testing-amd64-DVD-1.iso" /mnt/apt
echo "/home/luca/Software/Sistemi\040operativi/Linux/Debian/debian-testing-amd64-DVD-1.iso /mnt/apt iso9660 user,loop,nofail 0 0" >> /etc/fstab
mv "/etc/apt/sources.list" "/etc/apt/sources.list.old"
cp "altro/apt/sources.list.nocdrom" "/etc/apt/sources.list"
apt-cdrom -m add
cp "altro/apt/sources.list" "/etc/apt/sources.list" # http://forums.debian.net/viewtopic.php?f=10&t=116346#p547918

# Debian stable e unstable
cp "altro/apt/sources.list.d/debian-stable.list" "/etc/apt/sources.list.d/"
cp "altro/apt/sources.list.d/debian-unstable.list" "/etc/apt/sources.list.d/"
cp "altro/apt/sources.list.d/debian-experimental.list" "/etc/apt/sources.list.d/"
cp "altro/apt/debian.pref" "/etc/apt/preferences.d/"

# Dropbox
#gpg --no-default-keyring --keyring /usr/share/keyrings/dropbox.gpg --keyserver keyserver.ubuntu.com --recv-keys FC918B335044912E # Sub-process /usr/bin/sqv returned an error code (1), error message is: Error: Failed to parse keyring "/usr/share/keyrings/dropbox.gpg"  Caused by:     0: Reading "/usr/share/keyrings/dropbox.gpg": EOF     1: EOF
cp "altro/apt/dropbox.asc" "/etc/apt/keyrings/"
cp "altro/apt/sources.list.d/dropbox.list" "/etc/apt/sources.list.d/"

# Chrome
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E88979FB9B30ACF2
cp "altro/apt/sources.list.d/google-chrome.list" "/etc/apt/sources.list.d/"

# Insync
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ACCAF35C
cp "altro/apt/sources.list.d/insync.list" "/etc/apt/sources.list.d/"

# TeamViewer
# https://www.teamviewer.com/it/download/linux/
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys C5E224500C1289C0
cp "altro/apt/sources.list.d/teamviewer.list" "/etc/apt/sources.list.d/"

# Caprine
# https://github.com/sindresorhus/caprine#apt-repository-gemfury
cp "altro/apt/sources.list.d/caprine.list" /etc/apt/sources.list.d/

dpkg --add-architecture i386 # https://wiki.debian.org/Wine#Step_1:_Enable_multiarch
apt-get update
apt-get -y dist-upgrade
apt-get -y install aptitude


echo -e "\n*** altri gestori di pacchetti ***"
aptitude -y install apt-rdepends
aptitude -y install npm
aptitude -y install python3-pip
aptitude -y install python3-venv
aptitude -y install snapd

# https://flatpak.org/setup/Debian
aptitude -y install flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
chown -R luca:luca /var/lib/flatpak/app/com.github.xournalpp.xournalpp/current/active/export/share/applications
aptitude -y install xdg-desktop-portal-kde # https://github.com/flatpak/xdg-desktop-portal-gtk/issues/355


echo -e "\n*** KDE ***"
aptitude -y remove plasma-discover
aptitude -y remove plasma-welcome
aptitude -y install dolphin-plugins
aptitude -y install plasma-browser-integration
aptitude -y install plasma-workspace-wallpapers # sfondi
aptitude -y install network-manager-fortisslvpn

aptitude -y install kde-config-gtk-style-preview
aptitude -y install plasma-theme-oxygen

aptitude -y install sddm-theme-breeze


echo -e "\n*** aMule ***"
aptitude -y install amule


echo -e "\n*** Audacity ***"
aptitude -y install audacity


echo -e "\n*** Bless ***"
aptitude -y install bless


echo -e "\n*** Caprine ***"
aptitude -y install caprine


echo -e "\n*** castnow ***"
npm install castnow -g


echo -e "\n*** Chrome ***"
aptitude -y install google-chrome-stable


echo -e "\n*** Clementine ***"
aptitude -y install clementine


echo -e "\n*** debsnap ***"
aptitude -y install devscripts


echo -e "\n*** deemix ***"
python3 -m venv /opt/deemix
chown -R luca:luca /opt/deemix


echo -e "\n*** Dropbox ***"
aptitude -y install dropbox
echo "fs.inotify.max_user_watches=2097152" >> /etc/sysctl.conf # https://www.dropboxforum.com/t5/Create-upload-and-share/Unable-to-monitor-dropbox-folder/m-p/394557/highlight/true#M33846
echo "fs.inotify.max_user_instances=256" >> /etc/sysctl.conf


echo -e "\n*** Evince ***"
aptitude -y install evince


echo -e "\n*** Filelight ***"
aptitude -y install filelight


echo -e "\n*** FileZilla ***"
aptitude -y install filezilla


echo -e "\n*** Firefox ***"
aptitude -y remove firefox-esr
aptitude -y install firefox-l10n-it


echo -e "\n*** firmware ***"
aptitude -y install firmware-linux


echo -e "\n*** fish ***"
aptitude -y install fish


echo -e "\n*** fluidsynth ***"
aptitude -y install fluidsynth


echo -e "\n*** Frescobaldi ***"
aptitude -y install frescobaldi


echo -e "\n*** geogebra ***"
aptitude -y install geogebra


echo -e "\n*** gparted ***"
aptitude -y install gparted


echo -e "\n*** graphicsmagick-imagemagick-compat ***"
aptitude -y install graphicsmagick-imagemagick-compat


echo -e "\n*** gtk3-nocsd ***"
aptitude -y install gtk3-nocsd


echo -e "\n*** HandBrake ***"
aptitude -y install handbrake


echo -e "\n*** hardinfo ***" # mi sembra più completo di KDE Info Center (kinfocenter)
aptitude -y install hardinfo


echo -e "\n*** id3v2 ***"
aptitude -y install id3v2


echo -e "\n*** Inkscape ***"
aptitude -y install inkscape


echo -e "\n*** Insync ***"
aptitude -y install insync
mv /usr/lib/insync/./libstdc++.so.6 /usr/lib/insync/./libstdc++.so.6.bak # https://forums.insynchq.com/t/insync-not-working-with-ubuntu-jammy-fixed/18219/15


echo -e "\n*** IntelliJ IDEA ***"
snap install intellij-idea-community --classic


echo -e "\n*** jpegtran ***"
aptitude -y install libjpeg-turbo-progs


echo -e "\n*** K3b ***"
aptitude -y install k3b-i18n
aptitude -y install normalize-audio


echo -e "\n*** Jigdo ***"
aptitude -y install jigdo-file


echo -e "\n*** kcharselect ***"
aptitude -y install kcharselect


echo -e "\n*** kde-config-tablet ***"
aptitude -y install kde-config-tablet


echo -e "\n*** kdenlive ***"
aptitude -y install kdenlive


echo -e "\n*** kdocker ***"
aptitude -y install kdocker


echo -e "\n*** KeePassXC ***"
aptitude -y install keepassxc-minimal


echo -e "\n*** Kile ***"
aptitude -y install kile


echo -e "\n*** KolourPaint ***"
aptitude -y install kolourpaint


echo -e "\n*** KRDC ***"
aptitude -y install krdc
aptitude -y install freerdp2-wayland


echo -e "\n*** krop ***"
aptitude -y install krop


echo -e "\n*** ktorrent ***"
aptitude -y install ktorrent


echo -e "\n*** lame ***"
aptitude -y install lame


echo -e "\n*** libdvd-pkg ***"
aptitude -y install libdvd-pkg


echo -e "\n*** LibreOffice ***"
aptitude -y install hyphen-it
aptitude -y install openclipart


echo -e "\n*** LilyPond ***"
aptitude -y install lilypond


echo -e "\n*** loudgain ***"
aptitude -y install loudgain


echo -e "\n*** manpages ***" # http://superuser.com/a/40611
aptitude -y install manpages-posix-dev


echo -e "\n*** mediainfo-gui ***"
aptitude -y install mediainfo-gui


echo -e "\n*** meld ***"
aptitude -y install meld


echo -e "\n*** micro ***"
aptitude -y install micro
aptitude -y remove nano
update-alternatives --install /usr/bin/editor editor /usr/bin/micro 100


echo -e "\n*** midicsv ***"
aptitude -y install midicsv


echo -e "\n*** MultiWriter ***"
aptitude -y install gnome-multi-writer


echo -e "\n*** MuseScore ***"
aptitude -y install musescore3


echo -e "\n*** net-tools ***"
aptitude -y install net-tools
aptitude -y install nmap


echo -e "\n*** NetHogs ***"
aptitude -y install nethogs


echo -e "\n*** ntpdate ***"
aptitude -y install ntpdate


#echo -e "\n*** onedrive ***"
#aptitude -y install onedrive
#rm /lib/systemd/system/onedrive@.service
#rm /usr/lib/systemd/user/onedrive.service


echo -e "\n*** OpenJDK ***"
aptitude -y install default-jdk
aptitude -y install icedtea-netx


echo -e "\n*** OpenShot ***"
aptitude -y install openshot-qt
mv /usr/lib/python3/dist-packages/openshot_qt/windows/preferences.py /usr/lib/python3/dist-packages/openshot_qt/windows/preferences.py.bak
cp altro/preferences.py /usr/lib/python3/dist-packages/openshot_qt/windows/ # https://github.com/OpenShot/openshot-qt/issues/3276#issuecomment-595052453


echo -e "\n*** optipng ***"
aptitude -y install optipng


echo -e "\n*** opus-tools ***"
aptitude -y install opus-tools


echo -e "\n*** p7m ***" # https://github.com/eniocarboni/p7m
git clone --depth 1 https://github.com/eniocarboni/p7m /home/luca/
chown -R luca:luca /home/luca/p7m
cp /home/luca/p7m/bin/p7m /opt/
chown luca:luca /opt/p7m
# continua in script.sh


echo -e "\n*** pandoc ***"
aptitude -y install pandoc


echo -e "\n*** PDFsam ***"
aptitude -y install pdfsam


echo -e "\n*** pdftk ***"
aptitude -y install pdftk


echo -e "\n*** pinta ***"
aptitude -y install pinta


echo -e "\n*** PipeWire ***"
aptitude -y install pipewire-audio
cp altro/hdmi.conf /etc/modprobe.d/ # audio su HDMI https://bbs.archlinux.org/viewtopic.php?id=290824


echo -e "\n*** printer-driver-all ***"
aptitude -y install printer-driver-all
aptitude -y install printer-driver-cups-pdf


echo -e "\n*** Qt Creator ***"
aptitude -y install qtcreator
aptitude -y install gdb # https://askubuntu.com/a/612819/82269
aptitude -y install qt6-base-dev # No suitable kits found


echo -e "\n*** rar ***"
aptitude -y install rar


echo -e "\n*** Rclone ***"
aptitude -y install rclone


echo -e "\n*** scour ***"
aptitude -y install scour


echo -e "\n*** speedtest-cli ***"
aptitude -y install speedtest-cli


echo -e "\n*** Teams ***"
snap install teams-for-linux


echo -e "\n*** TeamViewer ***"
aptitude -y install teamviewer
apt-key del C5E224500C1289C0


echo -e "\n*** Telegram Desktop ***"
aptitude -y install telegram-desktop


echo -e "\n*** texlive ***"
aptitude -y install texlive-lang-italian
aptitude -y install texlive-latex-extra
aptitude -y install texlive-font-utils
aptitude -y install texlive-extra-utils # pdfcrop

#aptitude -y install xzdec # http://askubuntu.com/a/485945

# richiesti dall'estensione TexMaths di LibreOffice
#aptitude -y install dvipng # raccomandato da kile
#aptitude -y install dvisvgm # dipeso da texlive-binaries (?)


echo -e "\n*** Thunderbird ***"
aptitude -y install thunderbird-l10n-it
aptitude -y install birdtray


echo -e "\n*** unrar ***"
aptitude -y install unrar


echo -e "\n*** VirtualBox ***"
aptitude -y install virtualbox-qt
aptitude -y install virtualbox-guest-additions-iso
adduser luca vboxusers # https://wiki.debian.org/VirtualBox#You_are_not_a_member_of_the_.22vboxusers.22_group


echo -e "\n*** VLC ***"
aptitude -y install vlc
aptitude -y install vlc-plugin-fluidsynth # midi


echo -e "\n*** whatsdek ***"
snap install whatsdesk


echo -e "\n*** wine ***"
aptitude -y install wine
aptitude -y install wine32:i386
aptitude -y install winetricks
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections # http://askubuntu.com/a/25614
aptitude -y install ttf-mscorefonts-installer


echo -e "\n*** Wireshark ***"
echo wireshark-common wireshark-common/install-setuid select true | debconf-set-selections
aptitude -y install wireshark
adduser luca wireshark
echo "WARNING: Riavvia la sessione per poter usare Wireshark."


echo -e "\n*** WoeUSB ***" # https://www.how2shout.com/linux/how-to-install-woeusb-on-debian-10-buster-bootable-usb-creator/
# perché non uso i virtual environment: https://github.com/WoeUSB/WoeUSB-ng/issues/2
aptitude -y install libgtk-3-dev # https://stackoverflow.com/a/74835306
pip3 install --break-system-packages attrdict3 # ModuleNotFoundError: No module named 'attrdict' + https://github.com/wxWidgets/Phoenix/issues/2236
pip3 install --break-system-packages WoeUSB-ng


echo -e "\n*** xsane ***"
aptitude -y install xsane


echo -e "\n*** yt-dlp ***"
aptitude -y install yt-dlp
