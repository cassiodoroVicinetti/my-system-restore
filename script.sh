#!/bin/bash

case $1 in

  "backup")
  
echo -e "\n*** backup di aMule ***"
cp -r "/home/luca/.aMule" "nuovi/"

    ;;
    
    
  *)


mv "/home/luca.old/Documenti" /home/luca/
mv "/home/luca.old/Immagini" /home/luca/
mv "/home/luca.old/Musica" /home/luca/
mv "/home/luca.old/Programmazione" /home/luca/
mv "/home/luca.old/Risorse remote" /home/luca/
mv "/home/luca.old/Scaricati" /home/luca/
mv "/home/luca.old/Scrivania" /home/luca/
mv "/home/luca.old/Scuola" /home/luca/
mv "/home/luca.old/Software" /home/luca/
mv "/home/luca.old/Video" /home/luca/
cp "altro/.hidden" /home/luca/


sudo ./sudo.sh


ln -s /home/luca/Documenti/Script/update.sh /home/luca/.local/bin/aggiorna


echo -e "\n*** configurazione di Git ***"
git config --global user.name "Luca Ghio"
git config --global user.email "artghio@tiscali.it"
git config --global core.autocrlf input
git config --global core.safecrlf true
git config --global core.autocrlf false # http://stackoverflow.com/questions/20168639/git-commit-get-fatal-error-fatal-crlf-would-be-replaced-by-lf-in
echo -e "[alias]\nhist = log --pretty=format:'%h %ad | %s%d [%an]' --graph --date=short" >> /home/luca/.gitconfig


echo -e "\n*** configurazione di Vim ***"
echo "set number" > /home/luca/.vimrc


# echo -e "\n*** configurazione di Firefox ***"
# if [ -d "/home/luca/.mozilla" ]
# then
#   mv "/home/luca/.mozilla" "vecchi/.mozilla"
# fi
# cp -r "nuovi/.mozilla/" "/home/luca/"


echo -e "\n*** configurazione di aMule ***"
if [ -d "/home/luca/.aMule" ]
then
  mv "/home/luca/.aMule" "vecchi/.aMule"
fi
cp -r "nuovi/.aMule/" "/home/luca/"


echo -e "\n*** configurazione di p7m ***"
chmod 777 /opt/p7m
cp -r /home/luca/p7m/.config /home/luca/
cp -r /home/luca/p7m/.local /home/luca/
mkdir /home/luca/.local/bin
ln -s /opt/p7m /home/luca/.local/bin/p7m
xdg-mime default p7m.desktop application/pkcs7-mime
rm -r /home/luca/p7m


# echo -e "\n*** configurazione di texlive ***"
# # http://askubuntu.com/a/485945
# tlmgr init-usertree
# tlmgr update --all
# 
# tlmgr install arydshln
# 
# fmtutil --all # http://www.tex.ac.uk/FAQ-newlang.html


echo -e "\n*** deemix ***"
source /opt/deemix/bin/activate
pip install deemix
deactivate


echo -e "\n*** gtk3-nocsd per Wayland ***"
# https://github.com/PCMan/gtk3-nocsd/issues/55#issuecomment-700217741
cp "altro/gtk3-nocsd-wayland.sh" "/home/luca/.config/plasma-workspace/env/"


./manuale.sh
    
esac
